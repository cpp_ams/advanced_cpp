#include <iostream>
#include <map>
using namespace std;

int main() {

  map<string, int> ages;

  ages["Mike"] = 40;
  ages["Frank"] = 20;
  ages["John"] = 25;

  // pair<string, int> addToMap("Peter", 100);
  // ages.insert(addToMap);
  ages.insert(make_pair("Peter", 100));

  if (ages.find("Max") != ages.end()) {
    cout << "Max found" << endl;
  } else {
    cout << "Max not found" << endl;
  }

  for (map<string, int>::iterator it = ages.begin(); it != ages.end(); it++) {
    pair<string, int> age = *it;
    cout << age.first << ": " << age.second << endl;
  }

  for (map<string, int>::iterator it = ages.begin(); it != ages.end(); it++) {
    cout << it->first << ": " << it->second << endl;
  }

  return 0;
}
