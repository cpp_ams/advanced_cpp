#include <iostream>
using namespace std;

void test(int n) {
	cout << "Test: " << n << endl;
}

int main() {
	test(3);

	void (*pTest)(int);

	pTest = test;

	pTest(4);

	return 0;
}
