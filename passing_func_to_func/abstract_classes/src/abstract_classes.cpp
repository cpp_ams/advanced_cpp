#include <iostream>
using namespace std;

class Animal {
public:
	virtual void run() = 0;
	virtual void speak() = 0;
	virtual ~Animal() {};

};

class Dog: public Animal {
public:
	virtual void speak() {
		cout << "Woff!" << endl;
	}
};

class Labrador: public Dog {
public:
	Labrador() {
		cout << "new labrador" << endl;
	}
	virtual void run() {
		cout << "Labrador running" << endl;
	}
};

void test(Animal &a) {
	a.run();
}

int main() {

	Labrador labs[5];

	Labrador lab;
	lab.run();
	lab.speak();

	cout << endl;

	Animal *animals[5];
	animals[0] = &lab;
	animals[0]->speak();

	cout << endl;

	test(lab);

	return 0;
}
