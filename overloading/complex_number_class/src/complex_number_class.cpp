#include <iostream>
#include "Complex.h"

using namespace std;
using namespace ams;

int main() {

	Complex c1(2, 3);	// constructor
	Complex c2(c1);		// copy constructor

	Complex c3 = c2;	// copy constructor
	Complex c4;
	c4 = c3;			// overloaded assignment operator

	cout << c1 << endl;
	cout << &c1 << endl;
	cout << c2 << ": " << c3 << endl;
	cout << &c2 << ": " << &c3 << endl;
	cout << c4 << endl;
	cout << &c4 << endl;

	return 0;
}
