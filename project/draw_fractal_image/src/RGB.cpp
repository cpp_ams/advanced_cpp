/*
 * RGB.cpp
 *
 *  Created on: Sep 1, 2018
 *      Author: ansc
 */

#include "RGB.h"

namespace ams {

RGB::RGB(double r, double g, double b): r(r), g(g), b(b) {
}

RGB operator-(const RGB& first, const RGB& second) {
	return RGB(first.r - second.r, first.g -second.g, first.b - second.b);
}

} /* namespace ams */
