/*
 * RGB.h
 *
 *  Created on: Sep 1, 2018
 *      Author: ansc
 */

#ifndef RGB_H_
#define RGB_H_

namespace ams {

struct RGB {
	double r;
	double g;
	double b;

	RGB(double r, double g, double b);
};

RGB operator-(const RGB& first, const RGB& second);

} /* namespace ams */

#endif /* RGB_H_ */
