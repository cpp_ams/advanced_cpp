#include <fstream>
#include <iostream>
using namespace std;

int main() {

  string filename = "stats.txt";
  ifstream input;

  input.open(filename);

  if (!input.is_open()) {
    return 1;
  }

  while (!input.eof()) {
    string line;
    getline(input, line, ':');

    int population;
    input >> population;

    // input.get();
    input >> ws;
    if (!input) {
      break;
    }

    cout << line << " -- " << population << endl;
  }

  return 0;
}
