#include <fstream>
#include <iostream>
using namespace std;

int main() {

  string inFileName = "../../write/src/text.txt";
  ifstream inFile;

  inFile.open(inFileName);

  if (inFile.is_open()) {

    string line;

    while (!inFile.eof()) {
      getline(inFile, line);
      cout << line << endl;
    }
    inFile.close();
  } else {
    cout << "Cannot open file: " << inFileName << endl;
  }

  return 0;
}
