#include <iostream>

using namespace std;

// preprocessor directive:
// align this data on single byte boundaries, removes padding
#pragma pack(push, 1)
// to work more efficiently the struct will add padding, resulting in a size of
// 64
struct Person {
  char name[50];
  int age;
  double weight;
};
#pragma pack(pop)

int main() {

  cout << sizeof(Person) << endl;
  return 0;
}
