#include <iostream>
#include <vector>
using namespace std;

int main() {

	auto texts = {"one", "two", "three"};

	for (auto text: texts) {
		cout << text << endl;
	}

	vector<int> numbers;
	numbers.push_back(7);
	numbers.push_back(10);
	numbers.push_back(11);
	numbers.push_back(15);
	numbers.push_back(20);

	for (auto number: numbers) {
			cout << number << endl;
		}

	string hello = "Hello";

	for (auto c: hello) {
		cout << c << endl;
	}

	return 0;
}
