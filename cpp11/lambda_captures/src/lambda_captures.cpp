#include <iostream>
using namespace std;

int main() {

	int one = 1;
	int two = 2;
	int three = 3;

	// Capture one and two by val
	[one, two](){ cout << one << two << endl;}();

	// Capture all local vars by val
	[=](){ cout << one << two << endl;}();

	// Capture all local vars by val, but capture three by ref
	[=, &three](){ three = 7;  cout << one << two << endl;}();
	cout << three << endl;

	// Capture all local vars by ref
	[&](){ two = 4;  cout << one << two << endl;}();
	cout << two << endl;

	// Capture all local vars by ref, one by val
	[&, one](){ one = 3000;  cout << one << two << endl;}();
	cout << one << endl;

	return 0;
}
