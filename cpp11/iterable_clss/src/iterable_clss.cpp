#include <iostream>

#include "ring.h"

using namespace std;

int main() {

	ring<string> textring(3);

	textring.add("one");
	textring.add("two");
	textring.add("three");


	// c++ 98
	for (ring<string>::iterator it = textring.begin(); it != textring.end(); it++) {
		cout << *it << endl;
	}

	cout << endl;

	// c++ 11
	for (string value: textring) {
		cout << value << endl;
	}

	return 0;
}
